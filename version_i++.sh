#!/bin/bash

# Файл с версией чарта
CHART_FILE="./Chart.yaml"

# Извлечение текущей версии
CURRENT_VERSION=$(grep '^version:' $CHART_FILE | awk '{print $2}')
echo "Current version: $CURRENT_VERSION"

# Разделение версии на составляющие
IFS='.' read -r -a VERSION_PARTS <<< "$CURRENT_VERSION"
MAJOR=${VERSION_PARTS[0]}
MINOR=${VERSION_PARTS[1]}
PATCH=${VERSION_PARTS[2]}

# Увеличение версии
NEW_PATCH=$((PATCH + 1))
NEW_VERSION="$MAJOR.$MINOR.$NEW_PATCH"
echo "New version: $NEW_VERSION"

# Обновление версии в файле
sed -i "s/^version: .*/version: $NEW_VERSION/" $CHART_FILE

