
{{- define "namespace" -}}
{{-  (index .Values.chart.namespace .Values.global.env).name -}}
{{- end -}}

{{- define "ingress-host" -}}
{{-  (index .Values.ingress .Values.global.env).host -}}
{{- end -}}

{{- define "storageClass-name" -}}
{{-  (index .Values.storageClass .Values.global.env).name -}}
{{- end -}}


{{- define "PersistentVolume-name" -}}
{{-  (index .Values.persistentVolume .Values.global.env).name -}}
{{- end -}}

{{- define "PersistentVolume-storage" -}}
{{-  (index .Values.persistentVolume .Values.global.env).storage -}}
{{- end -}}

{{- define "PersistentVolume-path" -}}
{{-  (index .Values.persistentVolume .Values.global.env).path -}}
{{- end -}}

{{- define "PersistentVolume-node" -}}
{{-  (index .Values.persistentVolume .Values.global.env).node -}}
{{- end -}}


{{- define "persistentVolumeClaim-name" -}}
{{-  (index .Values.persistentVolumeClaim .Values.global.env).name -}}
{{- end -}}

{{- define "persistentVolumeClaim-storage" -}}
{{-  (index .Values.persistentVolumeClaim .Values.global.env).storage -}}
{{- end -}}


